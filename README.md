# django-acmeproxy

Relays lego httpreq requests to an upstream accepting httpreq requests.

Checks if the domain is allowed and checks if the request client is in an
allowed IP address space.

## Installation

Install prerequisites:

```bash
apt update && apt install -y \
    git \
    nginx-full \
    postgresql \
    python3-venv \
    python3-dev \
    build-essential \
    certbot \
    python3-certbot-nginx
```

Clone the git repository:

```bash
git clone git@gitlab.com:Cornelicorn/django-acmeproxy.git /opt/django_acmeproxy
```

If you want to use postgresql (recommended), create a database.
Replace `SECUREPASSWORD` with a random character string

```bash
systemctl enable --now postgresql.service
sudo -u postgres /usr/bin/psql -c "CREATE USER django_acmeproxy WITH ENCRYPTED PASSWORD 'SECUREPASSWORD';"
sudo -u postgres /usr/bin/psql -c "CREATE DATABASE django_acmeproxy;"
sudo -u postgres /usr/bin/psql -c "GRANT ALL PRIVILEGES ON DATABASE django_acmeproxy TO django_acmeproxy;"
```

Run `install.sh` to create a venv and run migrations on the database.
The script will ask you questions on the way for configuration.

Then reboot or run `systemctl restart nginx.service django_acmeproxy.service`

## Updates

To update, update the git repository, run install.sh and restart the app.

```bash
cd /opt/django_acmeproxy
git pull
./install.sh
systemctl restart django_acmeproxy.service
```
