#!/bin/bash -e

cd $(dirname $0)

echo "New install" > install.log

if [ -d venv ]
then
    echo "Removing old venv"
    rm -rf venv >> install.log
fi

echo "Creating new venv"
python3 -m venv venv >> install.log

source venv/bin/activate >> install.log

echo "Upgrading pip"
python3 -m pip install -U pip  >> install.log
echo "Installing wheel"
python3 -m pip install -U wheel  >> install.log
echo "Installing setuptools"
python3 -m pip install -U setuptools  >> install.log

echo "Installing dependencies"
python3 -m pip install -U -r requirements.txt  >> install.log

CONFIG=false
if ! [ -f django_acmeproxy/django_acmeproxy/local_settings.py ]
then
    while true
    do
        read -p "There is no previous local configuration. Do you want to copy the default and edit it? " yn
        case $yn in
            [Yy]* ) CONFIG=true; break;;
            [Nn]* ) echo "Not continuing, please add local_settings.py"; exit;;
            * ) echo "Please answer yes or no.";;
        esac
    done

    if [ -z "${EDITOR}" ]
    then
        read -p '$EDITOR is not specified, please enter the command for your editor: ' EDITOR
    fi

    cp django_acmeproxy/django_acmeproxy/local_settings.example.py django_acmeproxy/django_acmeproxy/local_settings.py
    $EDITOR django_acmeproxy/django_acmeproxy/local_settings.py
fi

echo "Applying migrations"
python3 django_acmeproxy/manage.py migrate  >> install.log
echo "Collecting static files"
python3 django_acmeproxy/manage.py collectstatic --no-input  >> install.log
echo "Removing stale contenttypes"
python3 django_acmeproxy/manage.py remove_stale_contenttypes --no-input  >> install.log

echo "Completed installation."
if "$CONFIG"
then
    while true
    do
        read -p "Since you are installing for the first time, do you want to run system/install_production.sh to configure systemd and nginx? " yn
        case $yn in
            [Yy]* ) INSTALL=true; break;;
            [Nn]* ) echo "If you want to do this at a later point, just run system/install_production.sh manually"; exit;;
            * ) echo "Please answer yes or no.";;
        esac
    done
    system/install_production.sh
fi
