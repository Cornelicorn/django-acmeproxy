from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin
from guardian.admin import GuardedModelAdmin

from api.models import IPNetworkACL, Domain

class DomainAdmin(SimpleHistoryAdmin, GuardedModelAdmin):
    pass

admin.site.register(IPNetworkACL)
admin.site.register(Domain, DomainAdmin)
