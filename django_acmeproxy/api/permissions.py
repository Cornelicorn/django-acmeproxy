import ipaddress
from rest_framework import permissions

from django_acmeproxy.settings import PASS_NETWORKS
from api.models import Domain



def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ipaddress.ip_address(ip)

class IPIsAuthorized(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        auth = False
        ip = get_client_ip(request)

        if PASS_NETWORKS:
            for network in PASS_NETWORKS:
                if ip in network:
                    auth = True
                    break

        if isinstance(obj, Domain):
            for acl in obj.acls.all():
                if ip in acl.network:
                    auth = True
                    break

        return auth
