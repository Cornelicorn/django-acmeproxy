import ipaddress

from django.db import models
from django.core.exceptions import ValidationError
from simple_history.models import HistoricalRecords
from guardian.models import UserObjectPermissionBase, GroupObjectPermissionBase


class IPNetworkACL(models.Model):
    network_txt = models.CharField('IP Network', max_length=45+1+3)

    history = HistoricalRecords()

    @property
    def network(self):
        return ipaddress.ip_network(self.network_txt)

    def clean(self):
        try:
            ipaddress.ip_network(self.network_txt)
        except ValueError:
            raise ValidationError('Host bits not empty')
        return super().clean()

    def __str__(self):
        return self.network_txt

class IPNetworkACLUserObjectPermission(UserObjectPermissionBase):
    content_object = models.ForeignKey(IPNetworkACL, on_delete=models.CASCADE)

class IPNetworkACLGroupObjectPermission(GroupObjectPermissionBase):
    content_object = models.ForeignKey(IPNetworkACL, on_delete=models.CASCADE)


class Domain(models.Model):
    name = models.CharField('Domain Name', max_length=253)
    allowed = models.BooleanField('Allow issueance for this domain', default=True)
    acls = models.ManyToManyField(IPNetworkACL)

    history = HistoricalRecords()

    def __str__(self):
        return self.name

class DomainUserObjectPermission(UserObjectPermissionBase):
    content_object = models.ForeignKey(Domain, on_delete=models.CASCADE)

class DomainGroupObjectPermission(GroupObjectPermissionBase):
    content_object = models.ForeignKey(Domain, on_delete=models.CASCADE)
