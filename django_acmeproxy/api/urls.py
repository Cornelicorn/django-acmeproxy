from django.urls import path
from rest_framework import routers
from api.views import *

urlpatterns = [
    path('lego/present', LegoACMEPresentView.as_view(), name='lego-present'),
    path('lego/cleanup', LegoACMECleanupView.as_view(), name='lego-cleanup'),
]

app_name = 'api'
