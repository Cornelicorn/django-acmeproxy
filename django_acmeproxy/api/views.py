import re
from django.core.exceptions import ObjectDoesNotExist

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.exceptions import NotFound, ParseError, PermissionDenied

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

import httpx

from django_acmeproxy.settings import FORWARD_ENDPOINT, FORWARD_AUTH
from api.permissions import IPIsAuthorized
from api.models import Domain

class LegoACMEPresentView(APIView):
    lego_action = 'present'
    authentication_classes = []
    permission_classes = [ IPIsAuthorized ]
    def get_queryset(self):
        try:
            fqdn = self.request.data['fqdn']
        except KeyError:
            raise ParseError('Could not determine fqdn')
        while '.' in fqdn:
            try:
                domain = Domain.objects.get(name=fqdn)
                self.check_object_permissions(self.request, domain)
                if not domain.allowed:
                    raise PermissionDenied('No certificates can be issued for this domain')
                return domain
            except ObjectDoesNotExist:
                pass
            fqdn = re.sub(r'^[^\.]*\.', '', fqdn)
        else:
            raise NotFound('The specified fqdn does not match any allowed domain')

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            'fqdn': openapi.Schema(type=openapi.TYPE_STRING, description='FQDN'),
            'value': openapi.Schema(type=openapi.TYPE_STRING, description='Token'),
        }
    ))
    def post(self, request):
        # run this to check permissions
        domain = self.get_queryset()
        url = f"{FORWARD_ENDPOINT}/{self.lego_action}"
        resp = httpx.post(url, data=request.data, auth=FORWARD_AUTH)
        return Response(data=resp.text, status=resp.status_code)


class LegoACMECleanupView(LegoACMEPresentView):
    lego_action = 'cleanup'
